import React from "react";
import Error from "./Error";

const Age = ({ errors, register }) => {
	return (
		<>
			<div className="age">
				<label htmlFor="age">Age</label>
				<input
					className={errors.age?.message ? "error-input" : ""}
					type="number"
					id="age"
					name="age"
					placeholder="Age"
					{...register("age", {
						valueAsNumber: true,
						pattern: {
							value: /^\d+$/,
							message: "Invalid age: only integer numbers are allowed",
						},
						min: {
							value: 0,
							message: "Age should not be negative value",
						},
						max: {
							value: 110,
							message: "Age should not exceed 110",
						},
					})}
				/>
			</div>

			<Error>
				{errors.age?.message && <p role="alert">{errors.age?.message}</p>}
			</Error>
		</>
	);
};

export default Age;
