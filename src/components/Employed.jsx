import React from "react";

const Employed = ({ register }) => {
	return (
		<>
			<div className="employed">
				<label htmlFor="employed">Employed</label>
				<input type="checkbox" {...register("employed")} />
			</div>
		</>
	);
};

export default Employed;
