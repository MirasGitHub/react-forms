import React from "react";

const FavoriteColor = ({ register, colorOptions }) => {
	return (
		<>
			<div className="color">
				<label htmlFor="favoriteColor">Favorite Color</label>

				<select
					name="favoriteColor"
					id="favoriteColor"
					{...register("favoriteColor")}
				>
					<option value="">------ Select Color -----</option>
					{colorOptions.map((color) => (
						<option
							className="color-option"
							key={color}
							value={color}
							name="color"
							id="color"
						>
							{color}
						</option>
					))}
				</select>
			</div>
		</>
	);
};

export default FavoriteColor;
