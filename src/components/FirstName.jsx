import React from "react";
import Error from "./Error";

const FirstName = ({ errors, register, fullNamePattern }) => {
	return (
		<>
			<div className="first-name">
				<label htmlFor="firstName">First Name</label>

				<input
					className={errors.firstName?.message ? "error-input" : ""}
					type="text"
					id="firstName"
					name="firstName"
					placeholder="First Name"
					{...register("firstName", {
						required: {
							value: true,
							message: "First Name required",
						},
						pattern: {
							value: fullNamePattern,
							message:
								"Invalid first name: Only alphabetic characters are allowed!",
						},
					})}
				/>
			</div>
			<Error>
				{errors.firstName?.message && (
					<p role="alert">{errors.firstName?.message}</p>
				)}
			</Error>
		</>
	);
};

export default FirstName;
