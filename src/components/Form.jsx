import { useForm } from "react-hook-form";
import { sauces } from "../data/sauces";
import { colorOptions } from "../data/colorOptions";
import { stooges } from "../data/stooges";
import { useEffect } from "react";
import FormOutput from "./FormOutput";
import FormButtons from "./FormButtons";
import FirstName from "./FirstName";
import LastName from "./LastName";
import Age from "./Age";
import Notes from "./Notes";
import Stooge from "./Stooge";
import FavoriteColor from "./FavoriteColor";
import Sauces from "./Sauces";
import Employed from "./Employed";

const fullNamePattern = /^[A-Za-z ]+$/;

const Form = () => {
	const {
		register,
		handleSubmit,
		reset,
		watch,
		getValues,
		formState: { errors, isSubmitSuccessful, isDirty, isValid },
	} = useForm({
		defaultValues: {
			stooge: "Larry",
			employed: false,
			favoriteColor: "",
			firstName: "",
			lastName: "",
			age: 0,
			sauces: [],
			notes: "",
		},
		mode: "onChange",
	});

	const formValues = getValues();

	const onSubmit = (data) => {
		if (isValid) {
			alert(`Form Submitted Successfully!, ${JSON.stringify(formValues)}`);
		}
	};

	const watchForm = watch();

	useEffect(() => {
		if (isSubmitSuccessful) {
			reset();
		}
	}, [isSubmitSuccessful, reset]);

	return (
		<div>
			<form className="form">
				<FirstName
					register={register}
					errors={errors}
					fullNamePattern={fullNamePattern}
				/>

				<LastName
					register={register}
					errors={errors}
					fullNamePattern={fullNamePattern}
				/>

				<Age register={register} errors={errors} />

				<Employed register={register} />

				<FavoriteColor register={register} colorOptions={colorOptions} />

				<Sauces register={register} sauces={sauces} />

				<Stooge register={register} stooges={stooges} />

				<Notes register={register} errors={errors} watch={watch} />

				<FormButtons
					handleSubmit={handleSubmit}
					isDirty={isDirty}
					onSubmit={onSubmit}
					reset={reset}
				/>
			</form>

			<FormOutput formValues={formValues} watchForm={watchForm} />
		</div>
	);
};

export default Form;
