const FormButtons = ({ handleSubmit, onSubmit, isDirty, reset }) => {
	return (
		<div className="btns">
			<button
				className="submit-btn"
				type="submit"
				onClick={handleSubmit(onSubmit)}
				disabled={!isDirty}
			>
				Submit
			</button>
			<button className="reset-btn" disabled={!isDirty} onClick={() => reset()}>
				Reset
			</button>
		</div>
	);
};

export default FormButtons;
