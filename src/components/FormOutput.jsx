const FormOutput = ({ formValues, watchForm }) => {
	return (
		<div className="form-values">
			<h4>Values: </h4>
			{formValues && <pre>{JSON.stringify(watchForm, null, 4)}</pre>}
		</div>
	);
};

export default FormOutput;
