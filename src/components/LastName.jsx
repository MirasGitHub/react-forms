import React from "react";
import Error from "./Error";

const LastName = ({ errors, register, fullNamePattern }) => {
	return (
		<>
			<div className="last-name">
				<label htmlFor="lastName">Last Name</label>
				<input
					className={errors.lastName?.message ? "error-input" : ""}
					type="text"
					id="lastName"
					name="lastName"
					placeholder="Last Name"
					{...register("lastName", {
						pattern: {
							value: fullNamePattern,
							message:
								"Invalid last name: Only alphabetic characters are allowed!",
						},
						required: {
							value: true,
							message: "Last Name required",
						},
					})}
				/>
			</div>
			<Error>
				{errors.lastName?.message && (
					<p role="alert">{errors.lastName?.message}</p>
				)}
			</Error>
		</>
	);
};

export default LastName;
