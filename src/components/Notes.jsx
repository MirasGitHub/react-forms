import React from "react";
import Error from "./Error";

const Notes = ({ errors, watch, register }) => {
	return (
		<>
			<div className="notes">
				<label htmlFor="notes">Notes</label>
				<textarea
					className={errors.notes?.message ? "error-input" : ""}
					name="notes"
					id="notes"
					cols="50"
					rows="2"
					onChange={() => watch("notes")}
					placeholder="Notes"
					{...register("notes", {
						maxLength: {
							value: 100,
							message: "Maximum 100 characters",
						},
					})}
				></textarea>
			</div>

			<Error>
				{" "}
				{errors.notes?.message && <p role="alert">{errors.notes?.message}</p>}
			</Error>
		</>
	);
};

export default Notes;
