import React from "react";

const Sauces = ({ sauces, register }) => {
	return (
		<>
			<div className="sauces">
				<label htmlFor="sauces">Sauces</label>
				<div>
					{sauces.map((sauce, index) => (
						<div className="checkbox-sauces" key={sauce.name}>
							<input
								type={sauce.type}
								value={sauce.name}
								{...register(`${sauce.checked ? `sauces.${index}` : "sauces"}`)}
							/>

							<label htmlFor="sauces">{sauce.name}</label>
						</div>
					))}
				</div>
			</div>
		</>
	);
};

export default Sauces;
