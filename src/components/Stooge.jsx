import React from "react";

const Stooge = ({ stooges, register }) => {
	return (
		<>
			<div className="best-stooge">
				<label htmlFor="stooge">Best Stooge</label>
				<div>
					{stooges.map((stooge) => (
						<div className="radio" key={stooge.name}>
							<input type="radio" {...register("stooge")} value={stooge.name} />
							<label htmlFor={stooge.name}>{stooge.name}</label>
						</div>
					))}
				</div>
			</div>
		</>
	);
};

export default Stooge;
