export const sauces = [
	{
		name: "Ketchup",
		checked: false,
		type: "checkbox",
	},
	{
		name: "Mustard",
		checked: false,
		type: "checkbox",
	},
	{
		name: "Mayonnaise",
		checked: false,
		type: "checkbox",
	},
	{
		name: "Guacamole",
		checked: false,
		type: "checkbox",
	},
];
