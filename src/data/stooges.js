export const stooges = [
	{
		name: "Larry",
		checked: true,
	},
	{
		name: "Moe",
		checked: false,
	},
	{
		name: "Curly",
		checked: false,
	},
];
